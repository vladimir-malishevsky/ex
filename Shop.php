<?php

include 'Toy.php';

class Shop
{
    private $toys;

    public function __construct()
    {
        $this->toys = [
            new Toy('Bear', 'м`яка', '2'),
            new Toy('Zhuravel', 'м`яка', '1'),
            new Toy('Monopoly', 'настільна', '8'),
            new Toy('Chicken', 'настільна', '2'),
        ];
    }

    public function getCountToysByType($type){
        $count = 0;
        foreach ($this->toys as $toy) {
            if($toy->getType() == $type)
                $count++;
        }
        return $count;
    }

    public function getCountToysByRecommendedYears($years){
        $count = 0;
        foreach ($this->toys as $toy) {
            if($toy->getRecommendedYears() == $years)
                $count++;
        }
        return $count;
    }
}