<?php


class Toy
{
    private $title;
    private $type;
    private $recommended_years;

    /**
     * Toy constructor.
     * @param $title
     * @param $type
     * @param $recommended_years
     */
    public function __construct($title, $type, $recommended_years)
    {
        $this->title = $title;
        $this->type = $type;
        $this->recommended_years = $recommended_years;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getRecommendedYears()
    {
        return $this->recommended_years;
    }

    /**
     * @param mixed $recommended_years
     */
    public function setRecommendedYears($recommended_years)
    {
        $this->recommended_years = $recommended_years;
    }

}